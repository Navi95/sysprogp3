#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#define SIZE 10

int validarNombre(char *);
int validarEdad(int);

int main()
{
	bool fin=false;
	while(fin==false)
	{	
		char nombre[SIZE];
		char apellido[SIZE];
		int edad;
		int edadTotal=0;
		char nombreMayor[SIZE];
		char apellidoMayor[SIZE];
		int edadMayor=0;
		char nombreMenor[SIZE];
		char apellidoMenor[SIZE];
		int edadMenor=131;
		
		for (int i=1; i<11; i++)
		{
			printf("Persona %d\n", i);
			printf("Ingrese su nombre: ");
			scanf("%s", nombre);
			printf("\n");
			if(validarNombre(nombre)==1)
			{
				printf("Ingrese su Apellido: ");
				scanf("%s", apellido);
				printf("\n");
				if(validarNombre(apellido)==1)
				{
					printf("Ingrese su edad: ");
					scanf("%d", &edad);
					printf("\n");
					if(validarEdad(edad)==1)
					{
						edadTotal+=edad;
						if(edad>edadMayor)
						{
							edadMayor=edad;
							strcpy(nombreMayor, nombre);
							strcpy(apellidoMayor, apellido);
						}
						if(edad<edadMenor)
						{
							edadMenor=edad;
							strcpy(nombreMenor, nombre);
							strcpy(apellidoMenor, apellido);			
						}				
					}else{printf("Edad no valida \nIngrese una edad entre 0 y 130\n");i--;}
				}else{printf("Apellido no valido\n");i--;}
			}else{printf("Nombre no valido\n");i--;}	
		}
		printf("Promedio de edad= %d\n", edadTotal/10);
		printf("Persona mas vieja: %s %s" , nombreMayor, apellidoMayor);
		printf(" Edad: %d\n", edadMayor);
		printf("Persona mas joven: %s %s", nombreMenor, apellidoMenor);
		printf(" Edad: %d\n", edadMenor);
		fin=true;
	}
}

int validarNombre(char *nombre)
{
	if (nombre[0]>=65 && nombre[0]<=90)
	{
		for(int i=1; i<SIZE; i++)
		{
			if(!(nombre[i]>=97 && nombre[i]<=122) && nombre[i]!=0 && nombre[i]!=-1)
			{
				return 0;
			}
		}
		return 1;
	}
	return 0;
}

int validarEdad(int edad)
{
	if(edad>0 && edad<130)
	{
		return 1;
	}
	return 0;
}
